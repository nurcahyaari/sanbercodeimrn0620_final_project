import React, {useEffect} from 'react';
import {View, Text, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

// costum component
import {List} from './components';
import {LoadingComponent} from '../../components/Loading';
// impor taction
import {GetSurahDetailAsync} from '../../../modules/redux/surah/surah.action';

export const Detail = () => {
  const nomorSurah = useSelector(state => state.surah.detail.nomor);
  const namaSurah = useSelector(state => state.surah.detail.nama);
  const surah = useSelector(state => state.surah.detail.data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(GetSurahDetailAsync(nomorSurah));
  }, [dispatch, nomorSurah]);

  return surah.length > 0 ? (
    <FlatList
      data={surah}
      renderItem={({item}) => <List {...item} />}
      keyExtractor={item => item.nomor}
      contentContainerStyle={{
        padding: 21,
      }}
      ListHeaderComponent={() => (
        <View style={{alignItems: 'center', marginVertical: 10}}>
          <Text style={{fontSize: 18}}>{namaSurah}</Text>
        </View>
      )}
    />
  ) : (
    <LoadingComponent />
  );
};
