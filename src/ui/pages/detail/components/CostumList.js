import React from 'react';
import {TouchableOpacity, View, Text, Alert} from 'react-native';
import {List as CostumList} from '../../../components/CostumList';
import HTML from 'react-native-render-html';

import {useNavigation} from '@react-navigation/native';

import {useDispatch} from 'react-redux';

// action
import {SetNomorSurah} from '../../../../modules/redux/surah/surah.action';

export const List = props => {
  const {ar, id, nomor, tr} = props;
  return (
    <>
      <CostumList>
        <View style={{marginRight: 15}}>
          <Text style={{fontWeight: 'bold'}}>{nomor}</Text>
        </View>
        <View style={{flexDirection: 'column', flex: 1}}>
          <View>
            <Text
              style={{
                textAlign: 'right',
                alignItems: 'flex-end',
                fontSize: 24,
              }}>
              {ar}
            </Text>
          </View>
          <View>
            <HTML html={tr} ptSize={2} />
          </View>
        </View>
        <View />
      </CostumList>
    </>
  );
};
