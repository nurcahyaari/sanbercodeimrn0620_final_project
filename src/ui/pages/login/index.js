import React from 'react';

import {StyleSheet, Text, ScrollView, View} from 'react-native';

// redux
import {useDispatch, useSelector} from 'react-redux';

import SizedBox from '../../components/SizedBox';
import Button from '../../components/Button';
import TextInput from '../../components/CostumTextInput';
import colors from '../../components/colors';

// action
import {
  SetUsername,
  SetPassword,
  LoginAsync,
} from '../../../modules/redux/auth/auth.action';

export const Login = () => {
  const email = useSelector(state => state.auth.username);
  const password = useSelector(state => state.auth.password);
  const info = useSelector(state => state.auth.info);

  const dispatch = useDispatch();

  const changeUsername = value => dispatch(SetUsername(value));
  const changePassword = value => dispatch(SetPassword(value));
  const login = () => {
    dispatch(LoginAsync(email, password));
  };

  console.log(email);
  console.log(info);

  return (
    <ScrollView contentContainerStyle={style.container}>
      <Text style={style.Loginheader}>Login</Text>
      <SizedBox margin={20} />
      {info.status === true ? (
        <View
          style={{
            height: 25,
            backgroundColor: 'red',
            justifyContent: 'center',
            borderRadius: 10,
            paddingHorizontal: 10,
          }}>
          <Text style={{color: '#fff'}}>{info.message}</Text>
        </View>
      ) : (
        <></>
      )}
      <TextInput
        placeholder="Email"
        textContentType="emailAddress"
        onChangeText={changeUsername}
        value={email}
        autoCapitalize="none"
      />
      <TextInput
        placeholder="Password"
        secureTextEntry
        onChangeText={changePassword}
        value={password}
        autoCapitalize="none"
      />
      <SizedBox margin={7} />
      <Button
        onPress={login}
        style={{backgroundColor: colors.primary}}
        title="Login"
      />
      <SizedBox margin={50} />
    </ScrollView>
  );
};

const style = StyleSheet.create({
  Loginheader: {
    fontSize: 27,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: 20,
  },
});
