import React from 'react';
import {TouchableOpacity, View, Text, Alert} from 'react-native';
import {List as CostumList} from '../../../components/CostumList';
import {useNavigation} from '@react-navigation/native';

import {useDispatch} from 'react-redux';

// action
import {SetNomorSurah} from '../../../../modules/redux/surah/surah.action';

export const List = props => {
  const navigaton = useNavigation();
  const dispatch = useDispatch();
  const {ayat, nama, nomor, asma, type} = props;
  let surahType = 'Madaniyah';
  if (type === 'mekah') {
    surahType = 'Makkiyah';
  }

  const gotoSurahDetail = () => {
    dispatch(SetNomorSurah(nomor, nama));
    navigaton.navigate('Detail');
  };

  return (
    <TouchableOpacity onPress={gotoSurahDetail}>
      <CostumList>
        <View>
          <Text>{nomor}</Text>
        </View>
        <View style={{flexDirection: 'column'}}>
          <View>
            <Text>{nama}</Text>
          </View>
          <View>
            <Text>
              {surahType} | {ayat} ayat
            </Text>
          </View>
        </View>
        <View>
          <Text style={{fontSize: 22}}>{asma}</Text>
        </View>
        <View />
      </CostumList>
    </TouchableOpacity>
  );
};
