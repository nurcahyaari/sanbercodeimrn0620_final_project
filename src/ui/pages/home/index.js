import React, {useEffect, useRef} from 'react';

import {
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Text,
  Image,
  View,
} from 'react-native';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';

import {useNavigation} from '@react-navigation/native';
// redux
import {useDispatch, useSelector} from 'react-redux';

import {GetSurahAsync} from '../../../modules/redux/surah/surah.action';

// custom component
import {List} from './components';
import colors from '../../components/colors';
import {LoadingComponent} from '../../components/Loading';
import {threedots} from '../../../assets';

export const Home = () => {
  const surah = useSelector(state => state.surah.list);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  let menuRef = useRef(null);

  useEffect(() => {
    console.log(surah.length);
    dispatch(GetSurahAsync());
  }, [dispatch, surah.length]);

  const setMenuRef = ref => (menuRef = ref);
  const showMenu = () => menuRef.show();
  // const hideMenu = () => menuRef.hide();
  const openAbout = () => {
    menuRef.hide();
    navigation.navigate('About');
  };

  return surah.length > 0 ? (
    <>
      <View
        style={{
          paddingHorizontal: 5,
          flexDirection: 'row',
          backgroundColor: colors.primary,
          justifyContent: 'center',
          alignItems: 'center',
          height: 50,
        }}>
        <Text
          style={{
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
            textAlign: 'right',
            color: '#fff',
            fontSize: 18,
          }}>
          Al Qur'an
        </Text>
        <TouchableOpacity
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            position: 'absolute',
            right: 5,
          }}>
          <Menu
            ref={setMenuRef}
            button={
              <Text onPress={showMenu}>
                <Image source={threedots} style={{width: 16, height: 16}} />
              </Text>
            }>
            <MenuItem onPress={openAbout}>About</MenuItem>
          </Menu>
        </TouchableOpacity>
      </View>

      <FlatList
        data={surah}
        renderItem={({item}) => <List {...item} />}
        keyExtractor={item => item.nomor}
        contentContainerStyle={styles.container}
      />
    </>
  ) : (
    <LoadingComponent />
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 21,
  },
});
