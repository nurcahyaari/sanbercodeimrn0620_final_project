import React from 'react';

import {View, Text} from 'react-native';

export const About = () => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
      <Text style={{textAlign: 'center'}}>Al-Qur’an mobile application</Text>
      <View
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
        <Text>Created By</Text>
        <Text>Mujahid Islami Prirmaldi. A</Text>
        <Text>Ari Nurchaya</Text>
      </View>
    </View>
  );
};
