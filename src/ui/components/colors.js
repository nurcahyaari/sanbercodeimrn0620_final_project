export default {
  primary: '#05AE20',
  danger: '#f06292',
  warning: '#ffd54f',
  black: '#0E1111',
  white: '#ffffff',
};
