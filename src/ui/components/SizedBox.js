import React from 'react';
import {View} from 'react-native';

const SizedBox = (props = {margin: 0}) => {
  const {margin} = props;
  return <View style={{margin}} />;
};

export default SizedBox;
