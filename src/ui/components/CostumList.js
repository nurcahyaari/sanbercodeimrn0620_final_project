import React from 'react';
import {View} from 'react-native';

export const List = props => {
  return (
    <View
      style={{
        marginVertical: 2,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      {props.children}
    </View>
  );
};
