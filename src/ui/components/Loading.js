import React from 'react';
import {View, Text} from 'react-native';

export const LoadingComponent = () => (
  <View
    style={{
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
    }}>
    <Text>Loading...</Text>
  </View>
);
