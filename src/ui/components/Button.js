/* eslint-disable no-unused-vars */
import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  TouchableOpacityProps,
  View,
  Text,
} from 'react-native';

const CustomButton = (Props = {title: '', style: ''}) => {
  const {title, style, ...props} = Props;
  return (
    <TouchableOpacity {...props} style={{...style, ...styles.Container}}>
      <Text>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    alignItems: 'center',
    borderRadius: 20,
    padding: 10,
  },
});

export default CustomButton;
