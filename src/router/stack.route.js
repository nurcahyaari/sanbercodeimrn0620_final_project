import {GenerateUID} from '../utils';

import {Login, Home, Detail, About} from '../ui/pages';

export const PublicRouter = [
  {
    id: GenerateUID(),
    name: 'Login',
    component: Login,
    options: {
      headerShown: false,
    },
  },
];

export const PrivateRouter = [
  {
    id: GenerateUID(),
    name: 'Home',
    component: Home,
    options: {
      headerShown: false,
    },
  },
  {
    id: GenerateUID(),
    name: 'Detail',
    component: Detail,
    options: {
      headerShown: false,
    },
  },
  {
    id: GenerateUID(),
    name: 'About',
    component: About,
    options: {
      headerShown: false,
    },
  },
];
