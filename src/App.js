/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';

// import redux
import {useSelector} from 'react-redux';

// import react navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {PublicRouter, PrivateRouter} from './router/stack.route.js';

const Stack = createStackNavigator();

const App = () => {
  const isLogin = useSelector(state => state.auth.status.isLogin);
  // const [IsLoading, setIsLoading] = useState(false);

  function renderLoginRoutes() {
    return (
      <NavigationContainer>
        {/* <Modal transparent animationType="fade" visible={IsLoading}>
          <View style={styles.modalContainer}>
            <ActivityIndicator size="large" />
          </View>
        </Modal> */}
        <Stack.Navigator>
          {PublicRouter.map(data => (
            <Stack.Screen
              key={data.id}
              name={data.name}
              component={data.component}
              options={data.options}
            />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  function renderLogedinRoutes() {
    return (
      <NavigationContainer>
        {/* <Modal transparent animationType="fade" visible={IsLoading}>
          <View style={styles.modalContainer}>
            <ActivityIndicator size="large" />
          </View>
        </Modal> */}
        <Stack.Navigator initialRouteName="Home">
          {PrivateRouter.map(data => (
            <Stack.Screen
              key={data.id}
              name={data.name}
              component={data.component}
              options={data.options}
            />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  return isLogin === true ? renderLogedinRoutes() : renderLoginRoutes();
};

export default App;
