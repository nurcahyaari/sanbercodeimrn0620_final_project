export default {
  baseURL: 'http://api-quran.herokuapp.com/',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
};
