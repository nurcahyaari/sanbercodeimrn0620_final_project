import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';

import AsyncStorage from '@react-native-community/async-storage';

// import reducers
import {combinedReducers} from '../../modules/redux';

// seting persist store
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: [],
};

const persistedReducer = persistReducer(persistConfig, combinedReducers);

export const Store = createStore(
  persistedReducer,
  compose(applyMiddleware(thunk)),
);

export const PersistStore = persistStore(Store);
