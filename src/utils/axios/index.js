import axios from 'axios';

// import config
import apiConfig from '../../config/api.config';

export default axios.create(apiConfig);
