import axios from '../../../utils/axios';

export const GetSurah = async () => {
  return axios.get('');
};

export const GetSurahDetail = async no => {
  return axios.get(`surah/${no}`);
};
