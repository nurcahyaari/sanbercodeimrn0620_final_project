import {
  GET_SURAH_LIST,
  GET_SURAH_DETAIL,
  FAILED_RESPONSE,
  SET_NOMOR_SURAH,
  RESET_NOMOR_SURAH,
} from './surah.constant';

const INITIAL_STATE = {
  list: [],
  detail: {
    nama: '',
    nomor: '',
    data: [],
  },
  info: {
    message: '',
    status: false,
  },
};

export const SurahReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SURAH_LIST: {
      return {
        ...state,
        list: action.payload.data,
        info: {
          message: '',
          status: false,
        },
      };
    }
    case SET_NOMOR_SURAH: {
      return {
        ...state,
        detail: {
          nama: action.payload.nama,
          nomor: action.payload.nomor,
          data: [],
        },
      };
    }
    case RESET_NOMOR_SURAH: {
      return {
        ...state,
        detail: {
          nama: '',
          nomor: '',
          data: [],
        },
      };
    }
    case GET_SURAH_DETAIL: {
      return {
        ...state,
        detail: {
          ...state.detail,
          data: action.payload.data,
        },
      };
    }
    case FAILED_RESPONSE: {
      return {
        ...state,
        info: {
          message: action.payload.message,
          status: true,
        },
      };
    }
    default: {
      return state;
    }
  }
};
