import {
  GET_SURAH_LIST,
  GET_SURAH_DETAIL,
  FAILED_RESPONSE,
  SET_NOMOR_SURAH,
} from './surah.constant';

// import services
import {GetSurah, GetSurahDetail} from './surah.services';

const getSurahList = data => ({
  type: GET_SURAH_LIST,
  payload: {
    data,
  },
});

const failedResponse = message => ({
  type: FAILED_RESPONSE,
  payload: {
    message,
  },
});

export const GetSurahAsync = () => async dispatch => {
  try {
    const res = await GetSurah();
    dispatch(getSurahList(res.data));
  } catch (e) {
    dispatch(failedResponse(e.message));
  }
};

export const SetNomorSurah = (nomor, nama) => ({
  type: SET_NOMOR_SURAH,
  payload: {
    nomor,
    nama,
  },
});

const getSurahDetail = data => ({
  type: GET_SURAH_DETAIL,
  payload: {
    data,
  },
});

export const GetSurahDetailAsync = nomor => async dispatch => {
  try {
    const res = await GetSurahDetail(nomor);
    dispatch(getSurahDetail(res.data));
  } catch (e) {
    dispatch(failedResponse(e.message));
  }
};
