export const GET_SURAH_LIST = 'surah/GET_SURAH_LIST';
export const GET_SURAH_DETAIL = 'surah/GET_SURAH_DETAIL';
export const FAILED_RESPONSE = 'surah/FAILED_RESPONSE';
export const SET_NOMOR_SURAH = 'surah/SET_NOMOR_SURAH';
export const RESET_NOMOR_SURAH = 'surah/RESET_NOMOR_SURAH';
