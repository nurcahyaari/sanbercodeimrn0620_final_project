export const SET_USERNAME = 'auth/SET_USERNAME';
export const SET_PASSWORD = 'auth/SET_PASSWORD';
export const USER_LOGIN = 'auth/USER_LOGIN';
export const USER_LOGOUT = 'auth/USER_LOGOUT';
export const WRONG_USER_OR_PASSWORD = 'auth/WRONG_USER_OR_PASSWORD';
