import {
  SET_USERNAME,
  SET_PASSWORD,
  USER_LOGIN,
  USER_LOGOUT,
  WRONG_USER_OR_PASSWORD,
} from './auth.constant';
import {GetUser} from './auth.services';

export const SetUsername = (username = '') => ({
  type: SET_USERNAME,
  payload: {
    username,
  },
});

export const SetPassword = (password = '') => ({
  type: SET_PASSWORD,
  payload: {
    password,
  },
});

const Login = () => ({
  type: USER_LOGIN,
  payload: null,
});

export const Logout = () => ({
  type: USER_LOGOUT,
  payload: null,
});

export const WrongUserOrPassword = () => ({
  type: WRONG_USER_OR_PASSWORD,
  payload: null,
});

export const LoginAsync = (username = '', password = '') => dispatch => {
  return setTimeout(() => {
    const user = GetUser(username, password);
    if (!user) {
      dispatch(WrongUserOrPassword());
      return;
    }
    dispatch(Login());
    return;
  }, 1000);
};
