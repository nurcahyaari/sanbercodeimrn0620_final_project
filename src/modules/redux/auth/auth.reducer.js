import {
  SET_USERNAME,
  SET_PASSWORD,
  USER_LOGIN,
  USER_LOGOUT,
  WRONG_USER_OR_PASSWORD,
} from './auth.constant';
import {GenerateUID} from '../../../utils';

const INITIAL_STATE = {
  username: '',
  password: '',
  status: {
    isLogin: false,
    token: '',
  },
  info: {
    status: false,
    message: '',
  },
};

export const AuthReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_USERNAME: {
      return {
        ...state,
        username: action.payload.username,
      };
    }
    case SET_PASSWORD: {
      return {
        ...state,
        password: action.payload.password,
      };
    }
    case USER_LOGIN: {
      return {
        ...state,
        username: '',
        password: '',
        status: {
          isLogin: true,
          token: GenerateUID(),
        },
        info: {
          status: false,
          message: '',
        },
      };
    }
    case WRONG_USER_OR_PASSWORD: {
      return {
        ...state,
        info: {
          status: true,
          message: 'username or password incorrect',
        },
      };
    }
    case USER_LOGOUT: {
      return INITIAL_STATE;
    }
    default: {
      return state;
    }
  }
};
