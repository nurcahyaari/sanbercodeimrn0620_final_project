import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
// import all reducer
import {AuthReducer} from './auth/auth.reducer';
import {SurahReducer} from './surah/surah.reducer';

// persist for auth
const authPersistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  whitelist: ['status'],
};

// persist for surah
const surahPersistConfig = {
  key: 'surah',
  storage: AsyncStorage,
  whitelist: ['list'],
};

export const combinedReducers = combineReducers({
  auth: persistReducer(authPersistConfig, AuthReducer),
  surah: persistReducer(surahPersistConfig, SurahReducer),
});
